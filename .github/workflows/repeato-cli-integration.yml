name: Integration with REPEATO CLI

on: [push]

jobs:
  repeato-cli:
    name: Build and run repeato tests using Repeato CLI
    runs-on: macos-latest

    steps:
      # Configure Node.js. Version 16 is currently required
      - uses: actions/setup-node@v3
        with:
          node-version: '16'

      # Check out the app source code
      - uses: actions/checkout@v3

      # Check out the Repeato test workspace. Your tests could also live in the same repository as your app source code. But in this case we decided to have it in it's separate repo.
      - name: Checkout workspace repo
        uses: actions/checkout@v3
        with:
          repository: repeato-qa/demo-workspace
          path: workspace-tests
          clean: false

      # We need Flutter to build the app
      - name: Install Flutter
        uses: subosito/flutter-action@v2
        with:
          flutter-version: '3.0.1'
          channel: 'stable'
      - run: flutter pub get
      - run: flutter build apk --split-per-abi

      # Optional: Add the built apk to the build artifacts
      - name: Upload APK
        uses: actions/upload-artifact@v3
        with:
          name: release-apk
          path: build/app/outputs/flutter-apk/app-x86_64-release.apk

      # Optional: We also commit the built app to the repo. It's saving us some time because we can use the apk in other workflows directly.
      - name: List APKs
        run: ls -la build/app/outputs/flutter-apk/
      
      - name: Move APK to project root
        run: mv build/app/outputs/flutter-apk/*.apk .

      - name: Commit APK
        uses: EndBug/add-and-commit@v9
        with:
          author_name: Github Action
          author_email: stephan@repeato.app
          message: 'Added APK'
          add: '*.apk'
          
      # Finally: Start emulator, install the app and run the tests via npx @repeato/cli-testrunner
      - name: run tests
        uses: reactivecircus/android-emulator-runner@d7b53ddc6e44254e1f4cf4a6ad67345837027a66
        with:
          api-level: 29
          arch: x86_64
          force-avd-creation: false
          emulator-options: -no-snapshot-save -no-window -gpu swiftshader_indirect -noaudio -no-boot-anim -camera-back none
          disable-animations: true
          script: adb install ./app-x86_64-release.apk && npx @repeato/cli-testrunner --workspaceDir "${GITHUB_WORKSPACE}/workspace-tests" --batchId 0 --licenseKey ${{ secrets.REPEATO_LICENSE_KEY }} --outputDir "${GITHUB_WORKSPACE}/batch-report"

      # Optional: Upload the reports to some bucket so a link to the batch run report can be easily shared
      # For AWS S3 You must enable enable ACLs object writing for bucket.
      # Don't put your access keys in here. Use Github secrets for storing & accessing the keys.
      # You might want to use 'if: always()' in this step and following ones, so the report is also uploaded if your test batch run failed.
#      - name: Upload report to S3
#        uses: shallwefootball/s3-upload-action@master
#        with:
#          aws_key_id: $AWS_ACCESS_KEY
#          aws_secret_access_key: $AWS_ACCESS_SECRET_KEY
#          aws_bucket: $AWS_BUCKET
#          source_dir: batch-report
